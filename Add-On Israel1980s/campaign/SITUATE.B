///////////////////////////////////////////////////////////////
// Begin situation report here
///////////////////////////////////////////////////////////////
#IF_TACTICAL_ENGAGEMENT
#ELSE
#EOL
#FONT 14
<h2>
106
</h2>
#ENDFONT
#EOL
///////////////////////////////////////////////////////////////
// Show planned offensive, if any
///////////////////////////////////////////////////////////////
#IF_OFFENSIVE_PLANNED
#TAB 10
SHOW_PLANNED_OFFENSIVE
#EOL
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
// Show planned defensive, if any
///////////////////////////////////////////////////////////////
#IF_DEFENSIVE_PLANNED
#TAB 10
SHOW_PLANNED_DEFENSIVE
#EOL
#EOL
#ENDIF
#TAB 10
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 1 2 
CONTEXT_STR RD
#EOL
#TAB 10
232
#EOL
BEST_FEATURES
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 3 4
CONTEXT_STR RD
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 5 6
CONTEXT_STR RD t
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 7 19
CONTEXT_STR
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 8
CONTEXT_STR
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 7 19
CONTEXT_STR
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 14 22
CONTEXT_STR
#EOL
#TAB 10
CONTEXT_STR 2
#EOL
POTENTIAL_TARGETS
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 15 16 17 27 44
CONTEXT_STR
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 18 20 23 24 25 26
CONTEXT_STR RV
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 30 31 32 35 36 37 38 41
CONTEXT_STR
#EOL
#TAB 10
232
#EOL
BEST_FEATURES
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 39
CONTEXT_STR
TARGET_VEHICLE_NAME
#IF_TARGET_VEH_IS_MISSILE
235
#ELSE
236
#ENDIF
CONTEXT_STR 2
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 40
CONTEXT_STR
#EOL
ENEMY_SQUADRONS
#TAB 10
CONTEXT_STR 2
#EOL
#TAB 10
232
#EOL
BEST_FEATURES
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 45 46 47 48 49
CONTEXT_STR
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 50
CONTEXT_STR
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 51
CONTEXT_STR
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 52
CONTEXT_STR OS OO
TARGET_VEHICLE_NAME
#IF_TARGET_VEH_IS_MISSILE
235
#ELSE
236
#ENDIF
CONTEXT_STR 2
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 53
CONTEXT_STR OS OO
#EOL
#TAB 10
232
#EOL
BEST_FEATURES
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 54
CONTEXT_STR OS OO
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 55 56 57
CONTEXT_STR OS OO
#EOL
#TAB 10
232
#EOL
BEST_FEATURES
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 58 59
CONTEXT_STR OS OO
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 60
CONTEXT_STR
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 61
CONTEXT_STR
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#IF_CONTEXT_EQ 62 63
CONTEXT_STR
#EOL
#ENDIF
///////////////////////////////////////////////////////////////
#ENDIF
#ENDSCRIPT
