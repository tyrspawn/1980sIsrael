# Israel 1980s Theater of Operations for Falcon 4 BMS

With Fieters joining the German airforce and essentially retiring from flying for awhile, United Operations Airforce (UOAF) has started up a small mod team, with the blessing of Uri and the ITO team, to create an 80s theater, this is an update thread. 

The Israel 1980s theater is:

  - Krause - cat herder
  - Gasman - artist
  - Nighthawk - campaign/developer
  - Foxy - database work, weapons, aircraft

### Info

- [How to clone repo and get latest](https://docs.google.com/a/krauselabs.net/document/d/1_NDedX5rdDINvpE1a4n69wKH3G0cIc0IEBWFjY5T9Rs/edit?usp=drive_web)
- [BMS forum post](https://www.bmsforum.org/forum/showthread.php?32141-Israel-1980s-UOAF-edition)
- [Work board](https://trello.com/b/CAvgEJaW/1980s-theater)
- [Theater concept](https://docs.google.com/a/krauselabs.net/spreadsheets/d/1hq3W7fdU8qElw4jruC4ZNZNM8G2lJ47E2DcmnsR0zWE/edit?usp=drive_web): 

Join our discord:
https://discord.gg/N8pev3N


### Installation

Israel 1980s requires [Falcon BMS 4.33.3 U3](https://www.bmsforum.org/forum/forum.php) to run.

Make sure

```
Add-On Israel1980s\Terrdata\theaterdefinition\Israel1980s.tdf
```
Is in your file C:\Falcon BMS 4.33 U1\Data\Terrdata\theaterdefinition\theater.lst

Make sure the campaign folder is extracted like so: C:\Falcon BMS 4.33 U1\Data\Add-On Israel1980s


### Known Issues

Campaign #3, the 1980 campaign is currently in pre-alpha and is not recommended for play. 

Known bugs are also located on the Trello board above. Please check it before reporting a bug. 

### Special thanks to:
- ITO FF Dev team
- Polak(Ted): original creator of terrain
- Cloud 9: orginal creator of FF campaign/DB

### Thanks to:

- uri_ba
- Starrats
- Manos
- ccc1tw
- Elouda
- Nurse
- Blackburn
- Sagid
- 611-Eagle
- KL0083
- Mike_69th
- Riboyster


for permissions for reusing some of their amazing work!
